﻿using System;

namespace Compiler
{
    public class ErrorHandler
    {
        public void ParseError(Token errorToken)
        {
            Console.WriteLine("Parse ERROR! ");
            Console.Write(errorToken.T);
            Console.Write(errorToken.nl + " ");
            Console.ReadKey();
            Environment.Exit(1);
        }



        protected static void ScanError(char symbol, int line)
        {
            
            Console.WriteLine("Parse ERROR! ");
            Console.WriteLine("On Line " + line + " error in symbol " + symbol);           
            Console.ReadKey();
            Environment.Exit(1);
            
        }


        protected void StackOverflow()
        {
            Console.WriteLine("Stack ERROR!");
            Console.WriteLine("Stack Overflow Exception");
            Console.ReadKey();
            Environment.Exit(1);
        }
        
        protected void StackEmpty()
        {
            Console.WriteLine("Stack ERROR!");
            Console.WriteLine("Stack Empty Exception");
            Console.ReadKey();
            Environment.Exit(1);
        }
    }
}