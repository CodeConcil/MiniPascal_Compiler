﻿namespace Compiler
{
    using System;
    using System.Collections.Generic;
    using  System.IO;
    using System.Text;
    
    public class Scan : ErrorHandler
    {
        
        public static readonly List<List<string>> TableIdent = new List<List<string>>();
        public  static int Index;
        
        public static List<string> I = new List<string>(); // идентификатор
        public static List<int> C = new List<int>(); // целые числа
        public static List<string> L = new List<string>(); // литералы
        public static string[] D = { "==", ">=", "<=", "<>", ":=", "..", "++", "--"};
        public static char[] R = {'>', '<', '=', ';', '.', '+', '-', '*', '/', '(' , ')', '[', ']', ':', ','};
        public static string[] K = { "program", "var", "integer", "boolean", "char", "string", "array", "of",
                                      "begin", "end", "readln", "while", "do", "if", "then", "else", "true", "false", "writeln", "forthif", "end.","AND","OR","endif", "swap","fort","until","loop"};
        
        
        private static int _state = 0;
        private static string _buffer = "";
        private static string _buf = ""; // буфер строки файла
        private static int _symbolIterator; // итератор символов файла
        private static bool _isBeginComment; // переменная комментария
        private static bool _isFile = true; // переменная конца файла
        private static int _errorLine;
        
        
        private static List<Token> TokenList = new List<Token>();
        
        
        public static void Reader() // чтение файла и вывод токенов
        {
            try
            {
                using (var rldn = new StreamReader("ab.txt", Encoding.Default))
                {
                    Token token;
                    
                    while (!rldn.EndOfStream)
                    {
                            _buf = rldn.ReadLine() + '~';
                            _errorLine++;
                        
                            for (_symbolIterator = 0; _symbolIterator < _buf.Length; _symbolIterator++)
                            {
                                token = ScanText(_buf[_symbolIterator]);

                                if (token != null)                                                 
                                    TokenList.Add(token);
                               
                                    
                            }
                        
                        if (_isFile == false)
                            break;                                                                       
                    }
                    _symbolIterator = 0;
                }

            }
            
            catch (Exception ex)
            {
               Console.WriteLine(ex);              
            }

            foreach (var variable in TableIdent)
            {
                variable.Add("");
            }
                        
            Index = 0;
            TableIdent[0].Add("");
            TableIdent[0].Add("");
            
              var start = new Parser();
              Console.WriteLine("Scaner Done!");
              start.Programm();
            
        }


        public static Token NextToken()
        {
            return (_symbolIterator < TokenList.Count) ? TokenList[_symbolIterator++] : null;            
        }
        
        
        
        
        
        

        private static Token ScanText(char symbol)
        {
            
            var convertToInt = 0; // переменная приведения к int
            while (true)
            {
                
                switch (_state)
                {
                        
                    case 0: // проверка символа
                    {
                        if (symbol == ' ') // проверка на пробел
                        {
                            return null;
                        }

                        if (symbol == '~') // проверка на конец строки
                        {
                            
                            return null;
                        }

                        if (int.TryParse(symbol.ToString(), out convertToInt)) // проверка на число
                        {
                            _state = 1;
                            break;
                            
                        }


                        if ((symbol >= 'a' && symbol <= 'z') || (symbol >= 'A' && symbol <= 'Z') || (symbol == '_')) // проверка на букву
                        {
                            _state = 2;
                            break;
                        }

                        if (symbol == '"' || symbol.ToString() == "'") // проверка на литерал   
                        {
                            _state = 7;
                            break;
                        }

                        foreach (var variable in R)
                        {
                        
                            if (symbol == variable)
                                _state = 3;                             
                        }                                   

                        if (_state != 3) 
                            return null;


                        ScanError(symbol, _errorLine);
                        
                        break;    

                    }

                    case 1: // Проверка на число
                    {

                        if (int.TryParse(symbol.ToString(), out convertToInt))   _buffer += symbol;                    
                                                  
                        else
                        {
                            for (var j = 0; j < C.Count; j++)
                                if (_buffer == C[j].ToString())
                                {
                                    _state = 0;
                                    _buffer = "";
                                    _symbolIterator--;                                 
                                    return new Token('C', j);
                                }
 
                            C.Add(Convert.ToInt32(_buffer));
                            _state = 0;
                            _buffer = "";
                            _symbolIterator--;
                            return new Token('C', C.Count-1);

                        }

                        return null;

                    }


                    case 2: // Проверка на букву
                    {
                        
                        if (_buffer == "end" && symbol == '.')
                        {

                            _isFile = false;
                            _state = 0;
                            return  new Token('K', 20);
                        }
                        
                        if (_buffer == "end" && symbol == 'i')
                        {
                            _buffer += symbol;
                            return  null;
                        }
                        
                        if (_buffer != "") // проверка на ключевое слово
                        {
                            for (var j = 0; j < K.Length; j++)
          
                                if (_buffer == K[j])
                                {
                                    _state = 0;
                                    
                                    _buffer = "";
                                    _symbolIterator--;
                                    
                                    return new Token('K', j);
                                }
                        }

                        
                        
                        

                        if (_buffer != "") // проверка на идентификатор
                        {
                            if ((int.TryParse(symbol.ToString(), out convertToInt)) || (symbol == '_'))
                            {
                                _buffer += symbol;
                            }

                            else if (symbol >= 'a' && symbol <= 'z' || symbol >= 'A' && symbol <= 'Z')
                            {
                                if (!(int.TryParse(_buffer[_buffer.Length - 1].ToString(), out convertToInt)))
                                {                                    
                                    _buffer += symbol;
                                }
                                else
                                {
                                    for (var j = 0; j < I.Count; j++)
                                        if (_buffer == I[j])
                                        {
                                            
                                            _state = 0;
                                            _buffer = "";
                                            _symbolIterator--;
                                           
                                            return new Token('I', j);
                                        }
                                    
                                    I.Add(_buffer);
                                    TableIdent.Add(new List<string>());
                                    
                                    TableIdent[Index].Add(_buffer);
                                    
                                    Index++;
                                    _state = 0;
                                    _buffer = "";
                                    _symbolIterator--;
                                    return new Token('I', I.Count-1);
                                }
                            }
                            else
                            {
                                for (var j = 0; j < I.Count; j++)
                                    if (_buffer == I[j])
                                    {   
                                        _state = 0;
                                        _buffer = "";
                                        _symbolIterator--;
                                        return new Token('I', j);
                                    }

                                I.Add(_buffer);
                                TableIdent.Add(new List<string>());
                                TableIdent[Index].Add(_buffer);

                                Index++;
                                _state = 0;
                                _buffer = "";
                                _symbolIterator--;
                                return new Token('I', I.Count-1);
                            }
                        }

                        if (_buffer == "")                            
                            _buffer += symbol;


                        

                        return null;



                    }
                    case 3: // проверка на символ
                    {
                        
                        _buffer += symbol;      
                        if (_buffer.Length == 2) // проверка на двулит разделитель
                        {   
                            for (var j = 0; j < D.Length; j++)
                                if (_buffer == D[j])
                                {
                                    _state = 0;
                                    _buffer = "";
                                    return new Token('D', j);

                                }
                            
                            if (_buffer == "(*")
                            {
                                _state = 5;
                                _buffer = "";
                                return null;
                            }

                             if (_buffer == "//")
                            {
                                _state = 6;
                                _buffer = "";
                                return null;
                            }

                             // проверка на однолит разделитель
                            
                                _buffer = "";
                                _state = 4;
                                _symbolIterator -= 2;
                            


                        }
                        
                        return null;


                    }

                    case 4: //  проверка на однолит разделитель
                    {
                        _buffer += symbol;
                        for (var j = 0; j < R.Length; j++)
                            if (_buffer == R[j].ToString())
                            {
                                _state = 0;
                                _buffer = "";
                                return new Token('R', j);
                            }
                        
                        return null;
                    }

                    case 5: // проверка на комментарии
                    {
                        
                        if (symbol == '*')
                        {
                            _buffer += symbol;
                            _isBeginComment = true;  
                            
                            return null;
                        }
                        if (symbol == ')' && _isBeginComment)
                        {
                            _state = 0;
                            _buffer = "";
                            
                        }
                        else
                        {
                            _buffer = "";
                            _isBeginComment = false;
                            
                        }
                                           
                        return null;
                    }
                    case 6: // проверка на конец кареттки
                    {
                        
                        if (symbol == '~')
                        {
                           
                            _state = 0;
                            _buffer = "";
                        }

                        return null;
                    }
                    case 7: // проверка на литерал
                    {
                        _buffer += symbol;
                        if (_buffer[0] == '"' && symbol == '"' && _buffer.Length > 1)
                        {
                            var buffer2 = "";
                            for (var j = 1; j < _buffer.Length - 1; j++)
                            {
                                buffer2 += _buffer[j];
                            }
                            L.Add(buffer2);
                            _state = 0;
                            _buffer = "";
                            return new Token('L', L.Count-1);

                        }
                        
                        if (_buffer[0].ToString() == "'" && symbol.ToString() != "'")
                        {
                            L.Add(symbol.ToString());
                            _state = 0;
                            _buffer = "";
                            _symbolIterator++;
                            return new Token('L', L.Count-1);
                        }


                        return null;

                    }
                        

                }
                
            }

            
           
        }
        
    }
}