﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;

namespace Compiler
{
    public class Generator
    {
         private readonly List<string> _codeSegment = new List<string>();
         private  readonly List<string> _dataSegment = new List<string>();
        
        private int _value;
        private int _valueif = 0;

        public void GenerateProgram()
        {
            using (var writer = new StreamWriter("OnWrite2.txt", false, System.Text.Encoding.Default))
            {
                writer.WriteLine("org 100h");
                writer.WriteLine("jmp start");
                writer.WriteLine("endl db 10, 13, '$'");
            }
            
        }

        public void GenerateInc(MyStack<string> stack)
        {
            _codeSegment.Add("inc  " + stack.Pop());
        }

        public void GenerateDec(MyStack<string> stack)
        {
            _codeSegment.Add("dec  " + stack.Pop());
        }

       public void GenerateWriteln(MyStack<string> myStack)
       {
           _value++;
           var flag = false;
           var temp = myStack.Pop();
           
           foreach (var variable in Scan.L)              
               if (temp == variable)
               {
                   _dataSegment.Add("value" + _value + " db 10, 13," + '"' + temp + "$" + '"');
                   _codeSegment.Add("mov dx, value" + _value);
                   flag = true;
                   break;
               }               

           if (!flag)
               _codeSegment.Add("mov dx, [" + temp + "]");
                
              
                _codeSegment.Add("mov ah, 09h");
                _codeSegment.Add("int 21h");
                _codeSegment.Add("mov dx, endl");
                _codeSegment.Add("mov ah, 09");
                _codeSegment.Add("int 21h");  
            
        }


        public void GenerateReadln(MyStack<string> myStack)
        {
            var temp = myStack.Pop();
            _dataSegment.Add(temp + " db 80, 0, 80 dup(0), '$'");
            
            _codeSegment.Add("mov ah, 3fh");
            _codeSegment.Add("mov dx," + temp);
            _codeSegment.Add("int 21h");
        }

        private int _valuewhile = 0;
        public void GenerateWhile(MyStack<string> myStack)
        {
           

            var k = myStack.Count;
            if (k > 1)
            {
                var temp = new List<string>();
                
                for (var i = 0; i < k; i++)
                    temp.Add(myStack.Pop());


                temp.Reverse();


                _codeSegment.Add("mov bx, [" + temp[0] + "]");
                _codeSegment.Add("@mywhile" + _valuewhile + ":");
                _codeSegment.Add("cmp bx," + temp[temp.Count - 1]);

                if (temp[1] == ">")
                {
                    _expression = "@endwhile" + _valuewhile.ToString();
                    _codeSegment.Add("jl " + _expression);
                }
                if (temp[1] == "<")
                {
                    _expression = "@endwhile" + _valuewhile.ToString();
                    _codeSegment.Add("jg " + _expression);
                }
                if (temp[1] == "==")
                {
                    _expression = "@endwhile" + _valuewhile.ToString();
                    _codeSegment.Add("jnz " + _expression);
                }
            }
            else
            {
                var boolean = myStack.Pop();
                
                if (boolean == "true")
                    _codeSegment.Add("@mywhile" + _valuewhile + ":");
              
                if (boolean == "false")
                {
                    _codeSegment.Add("jmp @endwhile"+_valuewhile);
                    _codeSegment.Add("@mywhile" + _valuewhile + ":");
                }
            }
            
            
        }

        public void GenerateEndIfFort()
        {
            _codeSegment.Add("endifFort:");
        }
        
       
        public void GenerateEndIf(MyStack<string> myStack)
        {
            
            _codeSegment.Add("jmp @endif"+_valueif.ToString());
           
        }
        public void GenerateEndElse(MyStack<string> myStack)
        {
            if (myStack.Pop() == "notelse")
            {
                _codeSegment.Add("@else" + _valueif.ToString() + ":");
            }
            _codeSegment.Add("@endif"+_valueif.ToString()+":");
            _valueif++;
        }
        public void GenerateEndWhile(MyStack<string> myStack)
        {
            _codeSegment.Add("jmp @mywhile"+_valuewhile);
            _codeSegment.Add("@endwhile"+_valuewhile+":");

            _valuewhile++;
        }

        private string _expression;
        private bool _and = false;
        private bool _or = false;
        
        public void GenerateIf(MyStack<string> myStack)
        {
            List<string> temp = new List<string>();
            
            int k = myStack.Count;
            
            if (k > 1)
            {          
                int count = 0;
                for (int i = 0; i < k; i++)
                {
                    temp.Add(myStack.Pop());
                    if (temp[temp.Count - 1] == "AND")
                    {
                        count++;
                        _and = true;
                    }
                    if (temp[temp.Count - 1] == "OR")
                    {
                        count++;
                        _or = true;
                    }


                }
                
                temp.Reverse();
                
                List<string> temp2 = new List<string>();
                int expressionIterator = 0;
                int countAndOr = 0;
                do
                {
                    
                    if (_and)
                    {
                        temp2 = new List<string>();
                        for (int j = 0; j < 3; j++)
                        {
                            temp2.Add(temp[countAndOr]);

                            countAndOr++;
                        }
                        countAndOr++;

                        _codeSegment.Add("mov ax, [" + temp2[0] + "]");
                        _codeSegment.Add("mov bx, [" + temp2[2] + "]");
                        _codeSegment.Add("cmp ax,bx");
                        if (temp2[1] == ">")
                        {
                            _expression = "@expression" + expressionIterator.ToString() + _valueif.ToString();
                            _codeSegment.Add("jg " + _expression);
                        }
                        if (temp2[1] == "<")
                        {
                            _expression = "@expression" + expressionIterator.ToString() + _valueif.ToString();
                            _codeSegment.Add("jl " + _expression);
                        }
                        if (temp2[1] == "==")
                        {
                            _expression = "@espression" + expressionIterator.ToString() + _valueif.ToString();
                            _codeSegment.Add("je " + _expression);
                        }
                        _codeSegment.Add("jmp @else" + _valueif.ToString());
                        _codeSegment.Add(_expression);
                    }
                    else if (_or)
                    {
                        temp2 = new List<string>();
                        for (int j = 0; j < 3; j++)
                        {
                            temp2.Add(temp[countAndOr]);

                            countAndOr++;
                        }
                        countAndOr++;

                        _codeSegment.Add("mov ax, [" + temp2[0] + "]");
                        _codeSegment.Add("mov bx, [" + temp2[2] + "]");
                        _codeSegment.Add("cmp ax,bx");
                        if (temp2[1] == ">")
                        {
                            _expression = "@espression" + _valueif.ToString();
                            _codeSegment.Add("jg " + _expression);
                        }
                        if (temp2[1] == "<")
                        {
                            _expression = "@espression" + _valueif.ToString();
                            _codeSegment.Add("jl " + _expression);
                        }
                        if (temp2[1] == "==")
                        {
                            _expression = "@espression" + _valueif.ToString();
                            _codeSegment.Add("je " + _expression);
                        }

                        if (expressionIterator == count)
                        {
                            _codeSegment.Add("jmp @else" + _valueif.ToString());
                            _codeSegment.Add("@expression" + _valueif.ToString() + ":");
                        }

                    }
                    else
                    {
                        _codeSegment.Add("mov ax, [" + temp[0] + "]");
                        _codeSegment.Add("mov bx, [" + temp[2] + "]");
                        _codeSegment.Add("cmp ax,bx");
                        if (temp[1] == ">")
                        {
                            _expression = "@expression" + _valueif.ToString();
                            _codeSegment.Add("jg " + _expression);
                        }
                        if (temp[1] == "<")
                        {
                            _expression = "@expression" + _valueif.ToString();
                            _codeSegment.Add("jl " + _expression);
                        }
                        if (temp[1] == "==")
                        {
                            _expression = "@espression" + _valueif.ToString();
                            _codeSegment.Add("je " + _expression);
                        }

                      
                            _codeSegment.Add("jmp @else" + _valueif.ToString());
                      
                            _codeSegment.Add("@expression" + _valueif.ToString() + ":");
                            
                        
                    }
                    expressionIterator++;

                } 
                while (expressionIterator < count + 1);


            }
            else
            {
                string gd = myStack.Pop();
                
                if (gd == "true")
                {
                    _codeSegment.Add("jmp expression"+_valueif);
                    _codeSegment.Add("@expression" + _valueif.ToString() + ":");
                }
                if (gd == "false")
                {
                    _codeSegment.Add("jmp @endif"+_valueif);
                }
                
            }

        }
        

        public void GenerateForthif(MyStack<string> myStack)
        {        
            List<string> temp = new List<string>();
            int k = myStack.Count;
            for (int i = 0; i < k; i++)
            {
                temp.Add(myStack.Pop());
            }
                       
            for (int i = 0; i < temp.Count; i++)
            {
                if (temp[i] == "+")
                {
                    
                    _codeSegment.Add("mov dx, [" + temp[i+1] + "]");
                    _codeSegment.Add("add dx, [" + temp[i-1] + "]");
                    _codeSegment.Add("mov ax, dx");
                    break;
                }
                else if (temp[i] == "-")
                {
                    
                    _codeSegment.Add("mov dx, [" + temp[i+1] + "]");
                    _codeSegment.Add("sub dx, [" + temp[i-1] + "]");
                    _codeSegment.Add("mov ax, dx");
                    break;
                }
                else if (temp[i] == "*")
                {
                    _codeSegment.Add("mov dx, [" + temp[i+1] + "]");
                    _codeSegment.Add("mul dx, [" + temp[i-1] + "]");
                    _codeSegment.Add("mov ax, dx");
                }
                else if (temp[i] == "/")
                {
                    _codeSegment.Add("mov dx, [" + temp[i+1] + "]");
                    _codeSegment.Add("div dx, [" + temp[i-1] + "]");
                    _codeSegment.Add("mov ax, dx");
                }
            }
         
        }


        private int forthCount = 0;
        
        public void GeneratePartForthIf()
        {
            
                _codeSegment.Add("cmp ax, 0");
                _codeSegment.Add("jl @forth0" + forthCount);

                _codeSegment.Add("cmp ax, 0");
                _codeSegment.Add("je @forth1" + forthCount);
         
                _codeSegment.Add("cmp ax, 0");
                _codeSegment.Add("jg @forth2" + forthCount);
        }

       public void GenerateMarkerForthIf(int value)
        {
            _codeSegment.Add("@forth" + value + forthCount + ":");
        }
        
       public void GenerateJumpForthIf(int value)
        {
            _codeSegment.Add("jmp @endforthif" + forthCount);
        }

        public void GenerateEndIfForth()
        {
            _codeSegment.Add("@endforthif" + forthCount + ":");
            forthCount++;
        }
        

        public void GenerateElse(MyStack<string> myStack)
        {
            
            _codeSegment.Add("@else"+_valueif.ToString()+":");
        }

        public List<string> ident = new List<string>();
        public void GenerateAssignmnet(MyStack<string> myStack)
        {
            
            bool data = false;
            try
            {
                
             List<string> temp = new List<string>();
             int k = myStack.Count;
            for (int i = 0; i < k; i++)
            {
                temp.Add(myStack.Pop());
                
            }
                
                bool d = false;
             
               
              

                for (int i = 0; i < ident.Count; i++)
                {
                    if (ident[i] == temp[temp.Count - 1])
                    {
                        data = true;
                      
                    }
                }
                if (ident.Count == 0)
                {
                    ident.Add(temp[temp.Count-1]);
                }
                if (!data)
                {
                    ident.Add(temp[temp.Count-1]);
                }
                
            string type="";
          
            for (int i = 1; i < Scan.TableIdent.Count-1; i++)
            {
                if (temp[temp.Count - 1] == Scan.TableIdent[i][0])
                {
                    type = Scan.TableIdent[i][1];
                    break;
                }
            }
             
                if (!data)
                {
                    if (type == "string" || type == "char")
                    {
                        
                        _dataSegment.Add(temp[temp.Count - 1] + " db '$'");

                        
                    }
                    else if (type == "integer" || type == "boolean")
                    {
                        
                        _dataSegment.Add(temp[temp.Count - 1] + " dw 0");
                        
                    }
                    else if (type == "array")
                    {
                        _dataSegment.Add(temp[temp.Count - 1] + " db 4");
                    }
                }
                if (type == "string" || type == "char")
                {

                    _codeSegment.Add("mov dx, ['" + temp[0] + "']");
                    _codeSegment.Add("mov word [" + temp[temp.Count - 1] + "], dx");
                    
                    AddinTable(temp[temp.Count-1], temp[0]);
             
                }
            for (int i = 0; i < temp.Count; i++)
            {
                if (temp[i] == "+")
                {
                    
                    _codeSegment.Add("mov dx, [" + temp[i+1] + "]");
                    _codeSegment.Add("add dx, [" + temp[i-1] + "]");
                    _codeSegment.Add("mov ax, dx");
                    _codeSegment.Add("mov word [" + temp[temp.Count - 1]+ "], ax");
                    
                    AddinTable(temp[temp.Count-1], temp[i-1], temp[i+1], '+');
                    
                }
               else if (temp[i] == "-")
                {
                    
                    _codeSegment.Add("mov dx, [" + temp[i+1] + "]");
                    _codeSegment.Add("sub dx, [" + temp[i-1] + "]");
                    _codeSegment.Add("mov ax, dx");
                    _codeSegment.Add("mov word [" + temp[temp.Count - 1]+ "], ax");
                    AddinTable(temp[temp.Count-1], temp[i-1], temp[i+1], '-');
                    
                }
                else if (temp[i] == "*")
                {
                    _codeSegment.Add("mov dx, [" + temp[i+1] + "]");
                    _codeSegment.Add("mul dx, [" + temp[i-1] + "]");
                    _codeSegment.Add("mov ax, dx");
                    _codeSegment.Add("mov word [" + temp[temp.Count - 1]+ "], ax");
                    AddinTable(temp[temp.Count-1], temp[i-1], temp[i+1], '*');
                }
                else if (temp[i] == "/")
                {
                    _codeSegment.Add("mov dx, [" + temp[i+1] + "]");
                    _codeSegment.Add("div dx, [" + temp[i-1] + "]");
                    _codeSegment.Add("mov ax, dx");
                    _codeSegment.Add("mov word [" + temp[temp.Count - 1]+ "], ax");
                    AddinTable(temp[temp.Count-1], temp[i-1], temp[i+1], '/');
                }
            }
            
            if (type == "integer" || type == "boolean")
            {
                _codeSegment.Add("mov ax, " + temp[0]);
                _codeSegment.Add("mov word [" + temp[temp.Count - 1] + "], ax");
                AddinTable(temp[temp.Count-1], temp[0]);
                
            }
                
        
            }
            catch (Exception e)
            {
               
            }
           
        }


        void AddinTable(string ident, string value, string value1 = null, char operand = ' ')
        {
            foreach (var VARIABLE in Scan.TableIdent)
            {

                if (VARIABLE[0] == ident)
                {

                    try
                    {
                        if (VARIABLE[2] == "")
                            if (value1 != null)
                            {
                                VARIABLE[2] = value + " " + operand + " " + value1;
                                VARIABLE.Add("");
                            }
                            else
                            {
                                VARIABLE[2] = " " + value;
                                VARIABLE.Add("");
                            }
                        else
                        {
                            if (VARIABLE[1] == "array")
                            {
                                if (value1 != null)
                                    VARIABLE[3] = value + " " + operand + " " + value1;
                                else
                                    VARIABLE[3] = " " + value;
                            }

                            else
                            {
                                if (value1 != null)
                                    VARIABLE[2] = value + " " + operand + " " + value1;
                                else
                                    VARIABLE[2] = " " + value;
                            }
                            
                            
                           
                        }
                          
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("EPTA");
                    }

                }
            }
        }






        public void GenerateSwap(MyStack<string> stack)
        {
            string temp1 = stack.Pop();
            string temp2 = stack.Pop();
            _codeSegment.Add("mov ax,["+temp1+"]");
            _codeSegment.Add("mov bx, ["+temp2+"]");
            _codeSegment.Add("mov dx, ax");
            _codeSegment.Add("mov ax, bx");
            _codeSegment.Add("mov bx, dx");
            
            _codeSegment.Add("mov word ["+temp1+"], bx");
            _codeSegment.Add("mov word ["+temp2+"], ax");
            
            
        }

        public void GenerateEnd()
        {

            foreach (var VARIABLE in Scan.TableIdent)
                if (VARIABLE[2] == "")
                    VARIABLE.Add("");
            
            
            using (StreamWriter writer = new StreamWriter("OnWrite2.txt", true, System.Text.Encoding.Default))
            {

                for (int i = 0; i < _dataSegment.Count; i++)
                {
                    writer.WriteLine(_dataSegment[i]);
                }
                writer.WriteLine("start:");
                
                for (int i = 0; i < _codeSegment.Count; i++)
                {
                    writer.WriteLine(_codeSegment[i]);
                }
                writer.WriteLine("int 16h");
                writer.WriteLine("int 20h");
                
            }

            using (StreamWriter writer = new StreamWriter("OnWriteTable.txt", false, System.Text.Encoding.Default))
            {
                foreach (var VARIABLE in Scan.TableIdent)
                    writer.WriteLine(VARIABLE[0] + " " + VARIABLE[1] + " " + VARIABLE[2] + " " + VARIABLE[3]);
            }

            Console.WriteLine("Generator Done!");
        }

        private int valuewhileUntil;

        public void generateEndMyWhileUntil(MyStack<string> stack)
        {
            string temp = stack.Pop();
            List<string> booleanexpression = new List<string>();

            for (int i = 0; i < 3; i++)
            {
                booleanexpression.Add(stack.Pop());
              
            }

            //booleanexpression.Reverse();
            if (temp == "while")
            {
                _codeSegment.Add("mov ax, [" + booleanexpression[0] + "]");
                _codeSegment.Add("mov bx, [" + booleanexpression[2] + "]");
                _codeSegment.Add("cmp ax,bx");
                if (booleanexpression[1] == ">")
                {
                    _expression = "@while" + _valueif.ToString();
                    _codeSegment.Add("jg " + _expression);
                }
                if (booleanexpression[1] == "<")
                {
                    _expression = "@while" + _valueif.ToString();
                    _codeSegment.Add("jl " + _expression);
                }
                if (booleanexpression[1] == "==")
                {
                    _expression = "@while" + _valueif.ToString();
                    _codeSegment.Add("je " + _expression);
                }
            }
            if (temp == "until")
            {
                _codeSegment.Add("mov ax, [" + booleanexpression[0] + "]");
                _codeSegment.Add("mov bx, [" + booleanexpression[2] + "]");
                _codeSegment.Add("cmp ax,bx");
                if (booleanexpression[1] == ">")
                {
                    _expression = "@until" + valuewhileUntil.ToString();
                    _codeSegment.Add("jl " + _expression);
                }
                if (booleanexpression[1] == "<")
                {
                    _expression = "@until" + valuewhileUntil.ToString();
                    _codeSegment.Add("jg " + _expression);
                }
                if (booleanexpression[1] == "==")
                {
                    _expression = "@until" + valuewhileUntil.ToString();
                    _codeSegment.Add("jnz " + _expression);
                }
            }
            valuewhileUntil++;
        }
        public void generateMyWhileUntil(MyStack<string> stack)
        {
   
            string temp = stack.Pop();
            
            if (temp == "while")
            {
                _codeSegment.Add("@while" + valuewhileUntil.ToString()+":");
            }
            if (temp == "until")
            {
                _codeSegment.Add("@until" + valuewhileUntil.ToString()+":");
            }
        }

    }
}