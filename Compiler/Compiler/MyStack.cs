﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Compiler
{
    public class MyStack<T> : ErrorHandler
    {
        private T[] elements;

        const int size = 256;
        private int count;
        
        public MyStack()
        {      
            elements = new T[size];
        }

        public int Count
        {
            get { return count; }          
        }

        public void Clear()
        {
                    if(IsEmpty)
                        return;

            for (var i = elements.Length - 1; i >= 0; i--)
            {
             
                if (!IsEmpty)
                    elements[--count] = default(T);
                

            }
        }

        private bool IsEmpty
        {
            get { return count == 0; }
            
        }

        private bool IsFull
        {
            get { return count == size; }
        }
        
        
        
        public void Push(T element)
        {
            if (IsFull)
                StackOverflow();

            elements[count++] = element;
            
        }

        public T Pop()
        {
            if (IsEmpty)
                StackEmpty();

            var element = elements[--count];
            elements[count] = default(T);

            return element;
        }
    }
}