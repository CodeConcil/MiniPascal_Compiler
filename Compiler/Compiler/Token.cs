﻿namespace Compiler
{
    public class Token
    {
        public Token(char T, int nl)
        {
            this.T = T;
            this.nl = nl;
        }
        public char T;
        public int nl;

    }
 }
