﻿using System;


namespace Compiler
{
    public class Parser : ErrorHandler
    {
        private  Token _token;
        private readonly MyStack<string> _stack = new MyStack<string>();
        private readonly Generator _generate = new Generator();
        
        public void Programm() // начало программы
        {
           
           _token =  Scan.NextToken();

            CheckKeyWord('K', 0);
            CheckIdent();
            CheckDelimiter('R', 3);
            
            _generate.GenerateProgram();
            
            Block();

            Console.WriteLine("Parser Done!");
            
            _generate.GenerateEnd();

        }


       private void Block()
        {
            DeclarationOfVariables(); // объявление переменных
            ExecutingTheProgramm(); // выполнение программы
        }

       private void DeclarationOfVariables() // объявление переменных
        {
            CheckKeyWord('K', 1);

            while (_token.nl != 8 || _token.T != 'K')
            {
                VariableType();
                
                CheckDelimiter('R', 13);
            
                CheckType();
            
                CheckDelimiter('R', 3);
            }
        }



      private void VariableType() // строка переменных
        {
            while (_token.nl != 13 || _token.T != 'R')
            {
               CheckIdent();
                try
                {
                    Scan.Index++;
                   
                }
                catch (Exception e)
                {
                   
                }
                
                if (_token.nl != 13 || _token.T != 'R')
                {
                    
                    CheckDelimiter('R', 14);
                   
                }
                
            }    
            
        }
       
      private  void VariableTypeWrite() // проверка на содержание строки для вывода
        {
            switch (_token.T)
            {
                case 'L':
                {
                    CheckLit(); 
                    break;
                }

                case 'I':
                {
                    CheckIdent();
                    break;
                }
                case 'C':
                {
                    CheckNumber();
                    break;
                }
                default:
                {
                    ParseError(_token);
                    break;
                }
            }
                      
            if (_token.nl != 10 || _token.T != 'R')
            {           
                CheckDelimiter('R', 14);
            }
            else if (_token.nl == 10 && _token.T == 'R')
            {
                
            }
            else               
                ParseError(_token);


          
        }

        private void CheckType() // определение типа
        {
            int[] typeMass = {2, 3, 4, 5, 6};
            CheckKeyWord(typeMass);
            
            if (_token.nl == 6 && _token.T == 'K')
            {
                CheckKeyWord('K', 6);   
                AddInTable(6);
                CheckArray();
            }         
        }


       private  void CheckArray() // определение массива
        {
            CheckDelimiter('R', 11);

            CheckNumber();

            CheckDoubleDelimiter(5);
            
            CheckNumber();

            CheckDelimiter('R', 12);
            
            CheckKeyWord('K', 7);
            Scan.Index++;

            CheckType();
        }
        
       private void ExecutingTheProgramm() // выполнение программы
        {
            CheckKeyWord('K', 8);

            while (_token.T != 'K' || _token.nl != 20)
            {
                _stack.Clear();
                CheckOperator();
            }

            CheckKeyWord('K', 20);
        }


       private void CheckOperator() // проверка оператора
        {

            switch (_token.T)
            {
                case 'K':
                {
                    switch (_token.nl)
                    {
                        case 10:
                        {

                            MyReadLine();
                            break;
                        }

                        case 11:
                        {

                            MyWhile();
                            break;
                        }
                        case 13:
                        {
                            MyifPascal();
                            break;

                        }
                        case 18:
                        {
                            MyWriteLine();
                            break;
                        }
                        case 19:
                        {
                            MyForthIf();
                            break;
                        }

                        case 24:
                        {
                            MySwap();
                            break;
                        }

                        case 25:
                        {
                            Myif();
                            break;
                        }
                        case 26:
                        {
                            MyInc();
                            break;
                        }
                        case 12:
                        {
                            MyWhileUntil();
                            break;
                        }

                        default:
                        {
                            ParseError(_token);
                            break;
                        }
                    }
                    
                    break;

                }
                    
                case 'I':
                {
                     Assignment();
                     break;
                }
                default:
                {
                    ParseError(_token);
                    break;
                }
            }
     
        }

        private void MyWhileUntil() // метод Do WHILE | UNTIL
        {
            
            CheckKeyWord('K',12);
            CheckDelimiter('R',9);
            var temp="";
            
            if (_token.nl == 11 && _token.T == 'K')
            {
                
                temp = Scan.K[_token.nl];

                CheckKeyWord('K',11);
             
            }
            else if (_token.nl == 26 && _token.T == 'K')
            {
                temp = Scan.K[_token.nl];
                CheckKeyWord('K',26);
            }
           
            _stack.Push(temp);
            CheckDelimiter('R',10);
            
            _generate.generateMyWhileUntil(_stack);
            _stack.Clear();
            BooleanExpression();
            
            var stackboolean = new MyStack<string>();

            var k = _stack.Count;
            
            for (var i = 0; i < k; i++)
            {
                var a = _stack.Pop();
                stackboolean.Push(a);
            }
            
            _stack.Clear();
            while (_token.nl != 27 || _token.T != 'K')
                CheckOperator();
              
           
            CheckKeyWord('K',27);
            CheckDelimiter('R',3);
            stackboolean.Push(temp);
            _generate.generateEndMyWhileUntil(stackboolean);
        
            stackboolean.Clear();
            _stack.Clear();

        }

        private void MyInc()
        {            
            CheckDoubleDelimiter(6);
            CheckDelimiter('R', 3);
            _generate.GenerateInc(_stack);
            _stack.Clear();
        }

        private void MyDec()
        {
            CheckDoubleDelimiter(7);
            CheckDelimiter('R', 3);
            _generate.GenerateDec(_stack);
            _stack.Clear();
        }

      
       private void MyReadLine() // ввод строки
        {
            CheckKeyWord('K', 10);         
            CheckDelimiter('R', 9);         
            CheckIdent();  
            CheckDelimiter('R', 10);  
            CheckDelimiter('R', 3);
            _generate.GenerateReadln(_stack);
            _stack.Clear();
        }


        private void MyWhile() // цикл с предусловием
        {
           CheckKeyWord('K', 11);
            
           CheckDelimiter('R',9);
           BooleanExpression();
           CheckDelimiter('R',10);
           CheckKeyWord('K', 12);
           CheckKeyWord('K', 8);
           _generate.GenerateWhile(_stack);
           _stack.Clear(); 
            
            
           while (_token.nl != 9 || _token.T != 'K')
                CheckOperator();
            
            
            
            CheckKeyWord('K', 9); 
            _generate.GenerateEndWhile(_stack);
            _stack.Clear();
            CheckDelimiter('R', 3);
        }


        private void Assignment() // оператор присвоения
        {
           
            CheckIdent();

            if (_token.T == 'D' && _token.nl == 6)
            {
                MyInc();
                return;
            }
            if (_token.T == 'D' && _token.nl == 7)
            {
                MyDec();
                return;
            }
            
            if (_token.T == 'R')
                CheckElArray();
            
            CheckDoubleDelimiter(4);

            Expression();
            
            CheckDelimiter('R', 3);
            _generate.GenerateAssignmnet(_stack);
            
            _stack.Clear();

        }

       private void Expression() // выражение
        {
            var isDuableToken = false; 
            
            while ((_token.T != 'R' || _token.nl != 3))
            {
                if (_token.T == 'K' && _token.nl == 14)
                    return;
                
                if (_token.T == 'I')
                {
                    CheckIdent();
                    
                    if (_token.T == 'R' && _token.nl == 11)
                        CheckElArray();

                    isDuableToken = false;
                    continue;
                }

                if (_token.T == 'C')
                {
                    CheckNumber();
                    isDuableToken = false;
                    continue;
                }

                if (_token.T == 'L')
                {
                    CheckLit();
                    continue;
                }

                if (_token.T == 'K')
                {

                    switch (_token.nl)
                    {
                        case 16:
                        {
                            CheckKeyWord('K', 16);
                            _stack.Push("1");
                            break;
                        }
                        case 17:
                        {
                            CheckKeyWord('K', 17);
                            _stack.Push("0");
                            break;
                        }

                        default:
                        {
                            ParseError(_token);
                            break;
                        }
                            
                    } 
                    continue;
                }

                if (_token.T == 'R')
                {
                    switch (_token.nl)
                    {
                        case 5:
                        {
                            if (!isDuableToken)
                            {
                                
                                CheckDelimiter('R', 5);
                                _stack.Push("+");
                                isDuableToken = true;
                                
                            }
                            
                            else                                
                                ParseError(_token);

                            break;
                        }

                        case 6:
                        {
                            if (!isDuableToken)
                            {
                                CheckDelimiter('R', 6);
                                _stack.Push("-");
                                isDuableToken = true;
                                
                            }
                            
                            
                            else
                                ParseError(_token);
                            
                            break;
                        }

                        case 7:
                        {
                            if (!isDuableToken)
                            {
                                CheckDelimiter('R', 7);
                                _stack.Push("*");
                                isDuableToken = true;
                                
                            }
                            
                            else
                                ParseError(_token);

                            break;
                        }

                        case 8:
                        {
                            if (!isDuableToken)
                            {
                                CheckDelimiter('R', 8);
                                _stack.Push("/");
                                isDuableToken = true;
                                
                            }
                            
                            else                          
                                ParseError(_token);
                            
                            break;
                        }

                        case 9:
                        {
                            if (!isDuableToken)
                            {
                                CheckDelimiter('R', 9);
                                isDuableToken = true;
                                
                            }
                            
                            else                      
                                ParseError(_token);
                            
                            break;
                        }

                        case 10:
                        {
                            if (!isDuableToken)
                            {
                                CheckDelimiter('R', 10);
                                isDuableToken = true;
                               
                            }
                            
                            else
                                ParseError(_token);
                            
                            break;
                        }

                        case 3:
                        {
                           break;
                        }
                        
                        default:
                        {
                            
                            ParseError(_token);
                            break;
                        }
                    }
                    
                    
                }
                else                   
                    ParseError(_token);
            }
           
        }


        private void MySwap()
        {
            
            CheckKeyWord('K', 24);
            CheckIdent();
            CheckDelimiter('R', 14);
            CheckIdent();
            
            _generate.GenerateSwap(_stack);
        }
       


        private void MyForthIf() // оператор условия фортрана
        {
            CheckKeyWord('K', 19);
            
            Expression();
            
            CheckKeyWord('K', 14);
          
            _generate.GenerateForthif(_stack);
            _generate.GeneratePartForthIf();
            
            _generate.GenerateMarkerForthIf(0);
            Assignment();
            _generate.GenerateJumpForthIf(0);
            
            _generate.GenerateMarkerForthIf(1);
            Assignment();
            _generate.GenerateJumpForthIf(1);
            
            _generate.GenerateMarkerForthIf(2);
            Assignment();
            _generate.GenerateJumpForthIf(2);
            
            _generate.GenerateEndIfForth();

           
            
            _stack.Clear();
            CheckKeyWord('K', 23);
            CheckDelimiter('R', 3);
            
        }

        private void Myif() // оператор условия
        {
           
            CheckKeyWord('K', 25);
           
            CheckDelimiter('R',9);
            BooleanExpression();
            CheckDelimiter('R',10);
            CheckKeyWord('K', 14);
            
            _generate.GenerateIf(_stack);
            _stack.Clear();
            while (_token.nl != 23 || _token.T != 'K')
                CheckOperator();
            
            CheckKeyWord('K', 23); 
            
            CheckDelimiter('R', 3);
            
            _generate.GenerateEndIf(_stack);
            
            if (_token.T == 'K' && _token.nl == 15)
                MyElse();


        }
        
        private void MyifPascal() // оператор условия
        {
            
           CheckKeyWord('K', 13);
            
           CheckDelimiter('R',9);
           
            BooleanExpression();
            CheckDelimiter('R',10);
           
            CheckKeyWord('K', 14);
            CheckKeyWord('K', 8);
            
            _generate.GenerateIf(_stack);
            
            _stack.Clear();
            while (_token.nl != 9 || _token.T != 'K')                
                CheckOperator();
          
            CheckKeyWord('K', 9); 
            
            CheckDelimiter('R', 3);
            
            _generate.GenerateEndIf(_stack);

            if (_token.T == 'K' && _token.nl == 15)               
                MyElse();
            
            else
            {               
                _stack.Clear();
                _stack.Push("notelse");
                _generate.GenerateEndElse(_stack);
                _stack.Clear();
            }


        }
        
        private void MyElse() // оператор "иначе"
        {
             
            CheckKeyWord('K', 15);
           
            CheckKeyWord('K', 8);
            _generate.GenerateElse(_stack);
            _stack.Clear();
            while (_token.nl != 9 || _token.T != 'K')              
                CheckOperator();
      
            CheckKeyWord('K', 9); 
            _stack.Clear();
            _stack.Push("else");
           
            _generate.GenerateEndElse(_stack);
            _stack.Clear();
            Console.WriteLine("xxxx");
            CheckDelimiter('R', 3);
           
           
        }

        private void BooleanExpression() // логическое выражение
        {
            if (_token.T == 'K' && _token.nl == 16)
            {
                _stack.Push(Scan.K[_token.nl]);
                CheckKeyWord('K',16);
                return;
            }
            if (_token.T == 'K' && _token.nl == 17)
            {
                _stack.Push(Scan.K[_token.nl]);
                CheckKeyWord('K',17);
                return;
            }
   
            Compared();
            BooleanSymbol();
            Compared();

            if (_token.T == 'K' && _token.nl == 21)
            {
                _stack.Push("AND");
                CheckKeyWord('K',21);
                BooleanExpression();
            }
            
            if (_token.T == 'K' && _token.nl == 22)
            {
                _stack.Push("OR");
                CheckKeyWord('K',22);
                BooleanExpression();
            }
        }


        void Compared() // переменные логического выражения
        {

            switch (_token.T)
            {
                case 'C':
                {
                    CheckNumber();
                    break;
                }
                case 'I':
                {
                    CheckIdent();
                    break;
                }
                case 'K':
                {
                    switch (_token.nl)
                    {
                        case 16:
                        {
                            CheckKeyWord('K', 16);
                            _stack.Push("1");
                            break;
                        }
                        case 17:
                        {
                            CheckKeyWord('K', 17);
                            _stack.Push("0");
                            break;
                        }
                        default:
                        {
                            ParseError(_token);
                            break;
                        }
                    }
                    
                    break;
                    
                }

                default:
                {
                    ParseError(_token);  
                    break;
                }
            }       
        }


        private void BooleanSymbol() // символ логического выражения
        {
            int[] delBool = {0, 1};
            int[] duableDelBool = {0, 1, 2, 3};

            foreach (var numb in delBool)
                if (numb == _token.nl && _token.T == 'R')
                {
                    CheckDelimiter('R', numb);
                    _stack.Push(Scan.R[numb].ToString());
                    return;
                }
            
            foreach (var numb in duableDelBool)
                if (numb == _token.nl && _token.T == 'D')
                {
                    
                    CheckDoubleDelimiter(numb);
                    _stack.Push(Scan.D[numb]);
                    return;
                }
            
            ParseError(_token);
            
        }
        
        
      private  void MyWriteLine() // вывод строки
        {
            CheckKeyWord('K', 18);
            
            CheckDelimiter('R', 9);
            
            
            while (_token.nl != 10 || _token.T != 'R')
                VariableTypeWrite();
                
            
            CheckDelimiter('R', 10);
            CheckDelimiter('R', 3);
            _generate.GenerateWriteln(_stack);
            _stack.Clear();
        }



      private  void CheckElArray()
        {
            
            CheckDelimiter('R', 11);

            switch (_token.T)
            {
                case 'I':
                {
                    CheckIdent();
                    break;
                }
                case 'C':
                {
                    CheckNumber();
                    break;
                }
                default:
                {
                    ParseError(_token);
                    break;
                }
            }           
            
            CheckDelimiter('R', 12);
        }
        
        

       private void CheckKeyWord(char nameList, int numberToken) // проверка на ключевое слово
        {
            if (_token.nl == numberToken && _token.T == nameList)
                _token = Scan.NextToken();

            else
                ParseError(_token);

            
        }

      private  void CheckKeyWord(int[] numberTokens) // проверка на массив
        {
            if (_token.T == 'K')
            {
                if (_token.nl == 6)
                    return;


                foreach (var number in numberTokens)
                    if (_token.nl == number)
                    {
                       AddInTable(number);   
                        _token = Scan.NextToken();
                        return;
                    }

                ParseError(_token);
            }

            else
                ParseError(_token);
        }


       private static void AddInTable(int numbToken)
        {
            string[] ar = {"","","integer", "boolean", "char", "string", "array"};
            try
            {
                
                if (Scan.Index < Scan.TableIdent.Count)
                {
                    if (Scan.Index != 1 && Scan.TableIdent[Scan.Index - 1][1] == "array")
                    {
                        Scan.TableIdent[Scan.Index-1][2] = ar[numbToken];
                        Scan.TableIdent[Scan.Index-1].Add("");
                        Scan.Index--;
                    }
                    
                    else
                    {
                        Scan.TableIdent[Scan.Index][1] = ar[numbToken];
                        
                        Scan.TableIdent[Scan.Index].Add("");
                    }
            
                   

                }
                else
                {
                    Scan.TableIdent[Scan.TableIdent.Count - 1][2] = ar[numbToken];
                  
                    Scan.TableIdent[Scan.TableIdent.Count - 1].Add("");
                }

                for (var i = 1; i < Scan.Index; i++)          
                    if (Scan.TableIdent[i][1] == "")
                    {
                        Scan.TableIdent[i][1] = ar[numbToken];
                        Scan.TableIdent[i].Add("");
                    }

                
            }
            catch (Exception e)
            {
                
            }
            



        }
        
        
        

       private void CheckDelimiter(char nameList , int numberToken) // проверка на разделитель
        {
        
            
            if (_token.nl == numberToken && _token.T == nameList)               
                _token = Scan.NextToken();

            else 
                ParseError(_token);
        }

      private  void CheckDoubleDelimiter(int numberToken) // проверка на двулитерный разделитель
        {
            if (_token.T == 'D')
            { 
                
                if (_token.nl == numberToken)
                {
                     
                     _token = Scan.NextToken();
                     return;
                }
                   
                
                ParseError(_token);                
            }
            
            else                
                ParseError(_token);
        }

       private void CheckIdent() // проверка на идентификатор
        {
            if (_token.T == 'I')
            {
                for (var i = 0; i < Scan.I.Count; i++)
                    if (_token.nl == i)
                    {
                       
                        _stack.Push(Scan.I[i]);
                        _token = Scan.NextToken();
                        return;
                    }
                
                ParseError(_token);
            }

            else
                ParseError(_token);
        }

    private void CheckLit() // проверка на литерал
        {
            if (_token.T == 'L')
            {
                for (var i = 0; i < Scan.L.Count; i++)
                    if (_token.nl == i)
                    {
                        _stack.Push(Scan.L[i]);
                        
                        _token = Scan.NextToken();
                        return;
                    }

               ParseError(_token);
            }

            else
                ParseError(_token);
        }

       private void CheckNumber() // проверка на число
        {
            if (_token.T == 'C')
            {
                for (var i = 0; i < Scan.C.Count; i++)
                    if (_token.nl == i)
                    {
                        _stack.Push(Scan.C[i].ToString());
                        _token = Scan.NextToken();
                        return;
                    }
                
               ParseError(_token);
            }

            else
                ParseError(_token);

            
        }
        
        
    }
}